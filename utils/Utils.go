package utils

import (
	"log"
	"regexp"

	"github.com/boltdb/bolt"
)

func CheckValidNumber(key string) bool {
	var validNumber = regexp.MustCompile(`^[+][3][8][0][0-9\\s\\(\\)\\+]{9}$`)
	val := validNumber.MatchString(key)
	return val
}

func OpenDB() (*bolt.DB, error) {
	db, err := bolt.Open("my.db", 0600, nil)
	if err != nil {
		log.Fatal("open db error", err)
		return db, err
	}
	return db, err
}
