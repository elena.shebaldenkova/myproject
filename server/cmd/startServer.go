package cmd

import (
	"context"
	"errors"
	"fmt"
	"log"
	"gitlab.com/elena.shebaldenkova/myproject/models"
	"net"
	"time"

	"github.com/boltdb/bolt"
	"github.com/golang/protobuf/proto"
	"github.com/spf13/cobra"
	"google.golang.org/grpc"
)

type server struct{}

const (
	port       = ":50051"
	BucketName = "PHONEBOOK"
)

var db *bolt.DB = nil

// startServerCmd represents the startServer command
var startServerCmd = &cobra.Command{
	Use: "startServer",

	Run: func(cmd *cobra.Command, args []string) {
		db, _ = bolt.Open("my.db", 0600, &bolt.Options{Timeout: 1 * time.Second})
		defer db.Close()
		lis, err := net.Listen("tcp", port)
		if err != nil {
			log.Fatalf("failed to listen: %v", err)
		}
		// Creates a new gRPC server
		s := grpc.NewServer()
		models.RegisterPhoneBookServer(s, &server{})
		if err := s.Serve(lis); err != nil {
			log.Fatalf("failed to serve: %v", err)
		}
		log.Println("Start server")
	},
}

func (server) AddNewRec(ctx context.Context, in *models.RecordFullRequest) (*models.RecordResponse, error) {
	err := db.Update(func(tx *bolt.Tx) error {
		bucket, err := tx.CreateBucketIfNotExists([]byte(BucketName))
		if err != nil {
			return fmt.Errorf("create bucket: %s", err)
		}
		if bucket.Get([]byte(in.Phone)) != nil {
			return errors.New("Record is present in DB")
		}
		if buf, err := proto.Marshal(in); err != nil {
			return errors.New("Marshaling error")
		} else if err := bucket.Put([]byte(in.Phone), buf); err != nil {
			fmt.Println(err)
			return errors.New("Writing in base error")
		}
		return nil
	})
	if err != nil {
		fmt.Println("begin error", err)
		return nil, err
	}
	return &models.RecordResponse{Phone: in.Phone, Status: true}, nil
}

func (server) FindRec(ctx context.Context, in *models.RecordRequest) (*models.RecordFullRequest, error) {
	record := &models.RecordFullRequest{}
	err := db.Update(func(tx *bolt.Tx) error {
		bucket := tx.Bucket([]byte(BucketName))
		if bucket == nil {
			return errors.New("Bucket not exist, start from addNewContact")
		}

		buf := bucket.Get([]byte(in.Phone))
		if err := proto.Unmarshal(buf, record); err != nil {
			log.Fatalln("Failed to parse record:", err)
		}
		return nil
	})
	if err != nil {
		log.Fatal("begin error", err)
		return nil, err
	}
	return record, nil
}

func (server) DeleteRec(ctx context.Context, in *models.RecordRequest) (*models.RecordResponse, error) {
	err := db.Update(func(tx *bolt.Tx) error {
		bucket := tx.Bucket([]byte(BucketName))
		if bucket == nil {
			return errors.New("Bucket not exist, start from addNewContact")
		}

		if next := bucket.Get([]byte(in.Phone)); next == nil {
			return errors.New("record is absent in DB")
		} else if err := bucket.Delete([]byte(in.Phone)); err != nil {
			return errors.New("error pending deleting")
		}

		return nil
	})
	if err != nil {
		return nil, err
	}
	return &models.RecordResponse{Phone: in.Phone}, nil
}
func (server) UpdateName(ctx context.Context, in *models.UpdateNameRequest) (*models.RecordResponse, error) {
	err := db.Update(func(tx *bolt.Tx) error {
		bucket := tx.Bucket([]byte(BucketName))
		if bucket == nil {
			return errors.New("Bucket not exist, start from addNewContact")
		}
		if next := bucket.Get([]byte(in.Phone)); next != nil {
			if buf, err := proto.Marshal(in); err != nil {
				return err
			} else if err := bucket.Put([]byte(in.Phone), buf); err != nil {
				return err
			}

		} else if next == nil {
			return errors.New("record is absent in DB")
		}

		return nil
	})
	if err != nil {
		return nil, err
	}
	return &models.RecordResponse{Phone: in.Phone, Status: true}, nil
}

func (server) ViewRecords(key *models.RecordKey, stream models.PhoneBook_ViewRecordsServer) error {
	err := db.View(func(tx *bolt.Tx) error {
		bucket := tx.Bucket([]byte(BucketName))
		if bucket == nil {
			return errors.New("Bucket not exist, start from addNewContact")
		}
		bucket.ForEach(func(k, v []byte) error {
			record := &models.RecordFullRequest{}
			if err := proto.Unmarshal(v, record); err != nil {
				log.Fatalln("Failed to parse note:", err)
			}

			if err := stream.Send(record); err != nil {
				return err
			}
			return nil
		})
		return nil
	})
	if err != nil {
		log.Fatal("begin error", err)
		return err
	}
	return nil
}

func init() {
	rootCmd.AddCommand(startServerCmd)
}
