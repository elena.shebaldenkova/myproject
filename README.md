Клиент-серверное приложение с использованием Сobra, Protobuf, gRPC

***Создание проекта***

Предварительно на пк должен быть установлен GO
при необходимости скачиваем необходимые исходники командами в консоли из директории $GOPATH\src
go get github.com/spf13/cobra/cobra
go get github.com/boltdb/bolt/...
go get github.com/golang/protobuf/protoc-gen-go
go get google.golang.org/grpc




1. Создать 2 проекта COBRA - серверную и клиентскую часть. 
Перейти в терминале %GOPATH%\bin и набрать команды:
cobra init %GOPATH%/[Путь к проекту/папка нашего проекта]/client 
cobra init %GOPATH%/[Путь к проекту/папка нашего проекта]/server 

2. Создать в корне нашего проекта в пакете models proto файл.
3. Прописать зависимости для протобуфа из директории $GOPATH/bin/protoc-3.5.0-win32/bin
   protoc -I=$GOPATH\src[имя проекта] --go_out=go_out=plugins=grpc:$GOPATH\src[имя проекта]\models\phonebook.proto $GOPATH\src[имя проекта] (после выполнения у вас в директории $GOPATH\src[имя проекта]\models сгенерится структура phonebook.pb.go)
4. - Перейти в каталог $GOPATH\src[имя проекта]/client и с помощью команды - cobra add [command] создаем команды, через которые пользователь будет отправлять необходимые команды серверу и отправлять запросы на сервер

   - Перейти в каталог $GOPATH\src[имя проекта]/server и с помощью команды - cobra add [startServer] - в этом файле будут находится функции, которые принимают с клиента запрос с аргументами и  работают с бд



***Запуск проекта***

Скачать проект - 
***go get gitlab.com/elena.shebaldenkova/myproject.git***


Перейти в консоли в пакет:  %GOPATH%/src/gitlab.com/elena.shebaldenkova/myproject.git/server и выполнить команду:

***go run main.go startServer***

(остановка сервера - в этом же терминале где он запущен и выполнить комбинацию клавиш "ctrl+C")



После чего открыть еще один терминал и перейти в пакет: %GOPATH%/gitlab.com/elena.shebaldenkova/myproject.git/client и выполнить необходимые команды:

***go run main.go [command]***

Где команды в формате:

addNewRecord +380631234567 Name City Street Build App                 - добавление новой записи [телефон в формате +380991234567] [Имя] [Город] [Улица] [номер дома] [Номер квартиры]

findRecord +380631234567                                               - поиск записи по номеру телефона [телефон в формате +380991234567] [Имя]

viewAllRecords                                                         - получение всей телефонной книги 

updateName +380631234567 Name                                          - изменение имени по телефону [телефон в формате +380991234567] [Имя]

deleteRecord +380631234567                                             - удаление записи по номеру телефона [телефон в формате +380991234567]


Тестирование будет добавлено до 23.12.2017
