package cmd

import (
	"context"
	"io"
	"log"
	"gitlab.com/elena.shebaldenkova/myproject/models"

	"github.com/spf13/cobra"
	"google.golang.org/grpc"
)

// viewAllRecordsCmd represents the viewAllRecords command
var viewAllRecordsCmd = &cobra.Command{
	Use:   "viewAllRecords",
	Short: "viewAllRecords",
	Run: func(cmd *cobra.Command, args []string) {
		conn, err := grpc.Dial(adress, grpc.WithInsecure())
		if err != nil {
			log.Fatalf("did not connect: %v", err)
		}
		defer conn.Close()
		client := models.NewPhoneBookClient(conn)
		key := &models.RecordKey{Phone: ""}

		stream, err := client.ViewRecords(context.Background(), key)
		if err != nil {
			log.Fatalf("bad stream: %v", err)
		}
		for {
			cicle, err := stream.Recv()
			if err == io.EOF {
				break
			}
			if err != nil {
				log.Fatalf("%v.viewAllRecords(_) = _, %v", client, err)
			}
			log.Printf("Record: %v", cicle)
		}

	},
}

func init() {
	rootCmd.AddCommand(viewAllRecordsCmd)
}
