package cmd

import (
	"context"
	"log"
	"gitlab.com/elena.shebaldenkova/myproject/models"

	"github.com/spf13/cobra"
	"google.golang.org/grpc"
)

// deleteRecordCmd represents the deleteRecord command
var deleteRecordCmd = &cobra.Command{
	Use:   "deleteRecord",
	Short: "deleteRecord +380631234567",

	Run: func(cmd *cobra.Command, args []string) {
		conn, err := grpc.Dial(adress, grpc.WithInsecure())
		if err != nil {
			log.Fatalf("not connect: %v", err)
		}
		defer conn.Close()

		client := models.NewPhoneBookClient(conn)

		rec := &models.RecordRequest{
			Phone: args[0],
		}

		resp, err := client.DeleteRec(context.Background(), rec)
		if err != nil {
			log.Println("Record not found ", err)
			return
		}
		log.Printf("Delete record: %v", resp)

	},
}

func init() {
	rootCmd.AddCommand(deleteRecordCmd)
}
