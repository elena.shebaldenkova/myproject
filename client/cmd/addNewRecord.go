package cmd

import (
	"context"
	"log"
	"gitlab.com/elena.shebaldenkova/myproject/models"
	"gitlab.com/elena.shebaldenkova/myproject/utils"

	"github.com/spf13/cobra"
	"google.golang.org/grpc"
)

const (
	adress = "localhost:50051"
)

// addNewRecordCmd represents the addNewRecord command
var addNewRecordCmd = &cobra.Command{
	Use:   "addNewRecord",
	Short: "addNewRecord +380631234567 Name City Street Build App",

	Run: func(cmd *cobra.Command, args []string) {
		conn, err := grpc.Dial(adress, grpc.WithInsecure())
		if err != nil {
			log.Fatalf("did not connect: %v", err)
		}
		defer conn.Close()

		client := models.NewPhoneBookClient(conn)
		if len(args) != 6 {
			log.Println("Invalid contact")
			return
		}

		if !utils.CheckValidNumber(args[0]) {
			log.Println("Invalid namber")
			return
		}

		record := &models.RecordFullRequest{
			Phone: args[0],
			Name:  args[1],
			Adress: &models.RecordFullRequest_Adress{
				Sity:       args[2],
				Street:     args[3],
				Building:   args[4],
				Appartment: args[5],
			},
		}

		resp, err := client.AddNewRec(context.Background(), record)
		if err != nil {
			log.Println("error in addContact():", err)
			return
		}
		if resp.Status == false {
			log.Println("This number exist:", resp.Phone)
			return
		}
		log.Println("Add contact:", resp.Phone)
	},
}

func init() {
	rootCmd.AddCommand(addNewRecordCmd)
}
