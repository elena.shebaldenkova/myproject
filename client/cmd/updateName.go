package cmd

import (
	"context"
	"log"
	"gitlab.com/elena.shebaldenkova/myproject/models"
	"gitlab.com/elena.shebaldenkova/myproject/utils"

	"github.com/spf13/cobra"
	"google.golang.org/grpc"
)

// updateNameCmd represents the updateName command
var updateNameCmd = &cobra.Command{
	Use:   "updateName",
	Short: "updateName +380631234567 Name",

	Run: func(cmd *cobra.Command, args []string) {
		conn, err := grpc.Dial(adress, grpc.WithInsecure())
		if err != nil {
			log.Fatalf("not connect: %v", err)
		}
		defer conn.Close()

		client := models.NewPhoneBookClient(conn)
		if len(args) != 2 {
			log.Println("bad contact - check count args")
			return
		}

		if !utils.CheckValidNumber(args[0]) {
			log.Println("Invalid number")
			return
		}

		record := &models.UpdateNameRequest{
			Phone: args[0],
			Name:  args[1],
		}

		resp, err := client.UpdateName(context.Background(), record)
		if err != nil {
			log.Println("error in update name:", err)
			return
		}
		if resp.Status == false {
			log.Println("This number is absent:", resp.Phone)
			return
		}
		log.Println("contact was updated:", resp.Phone)
	},
}

func init() {
	rootCmd.AddCommand(updateNameCmd)
}
